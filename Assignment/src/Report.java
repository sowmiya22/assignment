import java.util.Scanner;

public class Report {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter username: ");
		String userName = sc.nextLine();
		
		System.out.print("Enter password: ");
		String password = sc.nextLine();
		
		if ((userName.equals("sowmi")) && (password.equals("123"))) {
			System.out.println("welcome to teacher menu");
			Report menu1 =new Report();
			menu1.student_detail();
		}else {
			System.out.println("Invalid Login");;
				
		}

	}
	public void student_detail() {
		Scanner sc2 = new Scanner(System.in);
		System.out.print("Enter students total: ");
		int stu_number = sc2.nextInt();
		System.out.print("Enter subjects total: ");
		int sub_number = sc2.nextInt();
		
		String[][] stu_details=new String[stu_number][sub_number+2];
		
		for (int i=0; i<stu_number; i++) {
			System.out.print("Enter Student Name: ");
			String name=sc2.next();
			stu_details[i][0]= name;
			double total=0;
			for (int j=1;j<=sub_number; j++) {
				System.out.print("enter marks subject "+j +": ");
				String marks=sc2.next();
				stu_details[i][j]= marks;
				double d=Double.parseDouble(marks); 
				total=total+ d;
			}
			String str_total = Double.toString(total);
			stu_details[i][sub_number+1]=str_total;
		}
		System.out.println("press 1 see all students grade and total");
		System.out.println("press 2 see all high marks in each subject");
		int choice1=sc2.nextInt();
		if (choice1==1) {
			for (int i=0; i<stu_number; i++) {
				System.out.println("Student name is : "+stu_details[i][0]);
			
				for (int j=1; j<=sub_number; j++) {
			
					String marks = stu_details[i][j];
					double d=Double.parseDouble(marks);
					if (d>=75) {
						System.out.println("Grade for subject "+j+" is A");
					}else if (d>=65) {
						System.out.println("Grade for subject "+j+" is B");
					}else if (d>=55) {
						System.out.println("Grade for subject "+j+" is C");
					}else if (d>=35) {
						System.out.println("Grade for subject "+j+" is D");
					}else {
						System.out.println("Grade for subject "+j+" is F");
					}
					
				}
				System.out.println("Total for all subject is " + stu_details[i][sub_number+1]);
				System.out.println();
			}
		}else if(choice1==2) {
			System.out.print("choice subject number: ");
			int choice2= sc2.nextInt();
			
			String max = stu_details[0][choice2];
			double highmarks=Double.parseDouble(max);

			for (int i=1; i<stu_number;i++) {
				String mark = stu_details[i][choice2];
				double sub_marks=Double.parseDouble(mark);
				if (sub_marks>highmarks) {
					highmarks=sub_marks;
				}else {
					continue;
				}
			}
			System.out.print("Highest marks in subject "+ choice2 +" is "+ highmarks);
		}
	}

}




